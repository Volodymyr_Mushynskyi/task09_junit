package com.mushynskyi.tests;

import com.mushynskyi.tasks.Minesweeper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AssertsTest {

  private Minesweeper minesweeper;

  @Before
  public void createObject() {
    minesweeper = new Minesweeper(5);
  }

  @Test
  public void testRandomValue() {
    assertNotNull(minesweeper.getRandomValue());
  }

  @Test
  public void testEmptyString() {
    Assertions.assertEquals(minesweeper.getEmptyString(), "");
  }
}
