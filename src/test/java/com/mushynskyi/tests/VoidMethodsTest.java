package com.mushynskyi.tests;

import com.mushynskyi.tasks.Minesweeper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class VoidMethodsTest {

  private Minesweeper minesweeper;

  @Before
  public void init() {
    minesweeper = new Minesweeper(5);
  }

  @Test
  public void testVoidMethods() {
    minesweeper.countBombsCenterCase(1, 2);
    Assertions.assertNotNull(minesweeper.getCountBomb());
  }
}
