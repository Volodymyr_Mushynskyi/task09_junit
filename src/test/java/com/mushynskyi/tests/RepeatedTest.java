package com.mushynskyi.tests;

import com.mushynskyi.tasks.Plateau;
import org.junit.jupiter.api.Assertions;

public class RepeatedTest {

  private Plateau plateau;

  @org.junit.jupiter.api.RepeatedTest(5)
  public void testRandomValue() {
    plateau = new Plateau();
    plateau.randomInitialization();
    Integer[] firstArray = plateau.getArray();
    plateau = new Plateau();
    plateau.randomInitialization();
    Integer[] secondArray = plateau.getArray();
    Assertions.assertNotEquals(firstArray, secondArray);
  }
}
