package com.mushynskyi.tests;

import com.mushynskyi.tasks.Minesweeper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MocksTest {

  private Minesweeper minesweeper;

  @Before
  public void init() {
    minesweeper = new Minesweeper(5);
  }

  @Test
  public void mockTest() {
    Minesweeper mocSweeper = Mockito.spy(minesweeper);
    Mockito.doReturn(true).when(mocSweeper).checksGameOver(1, 2);
    Assert.assertEquals(true, mocSweeper.checkGame(1, 2));
  }
}
