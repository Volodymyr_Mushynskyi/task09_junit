package com.mushynskyi.tests;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class FileTest {
  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder();

  @Test
  public void testWrite() throws IOException {
    final File tempFile = tempFolder.newFile("tempFile.txt");
    FileUtils.writeStringToFile(tempFile, "some text");
    final String s = FileUtils.readFileToString(tempFile);
    Assertions.assertEquals("some text", s);
  }
}
