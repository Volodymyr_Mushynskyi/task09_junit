package com.mushynskyi.view;

import com.mushynskyi.tasks.Minesweeper;
import com.mushynskyi.tasks.Plateau;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Logger logger = LogManager.getLogger(View.class);
  private static Scanner input = new Scanner(System.in);

  public View() {
    menu = new LinkedHashMap<>();
    menu.put("1", "Press  1 - execute write command");
    menu.put("Q", "Press  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::printTasks);
  }

  private void printMenuAction() {
    logger.info("--------------MENU-----------\n");
    for (String str : menu.values()) {
      logger.info(str);
    }
  }

  public void showMenu() {
    String keyMenu ;
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      if(methodsMenu.containsKey(keyMenu)) {
        methodsMenu.get(keyMenu).print();
      }
    }while (!keyMenu.equals("Q"));
  }

  private void printTasks() {
    new Plateau();
    new Minesweeper().printMinesweeper();
  }
}