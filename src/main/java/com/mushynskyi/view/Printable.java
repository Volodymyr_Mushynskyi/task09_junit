package com.mushynskyi.view;

public interface Printable {
  void print();
}
