package com.mushynskyi.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Plateau {

  private int count = 1;
  private int end;
  private int trigger = 0;
  private int length;
  private int prev = 0;
  private int value;
  private Integer[] array = new Integer[50];
  private static Logger logger = LogManager.getLogger(Plateau.class);


  public Plateau() {
    randomInitialization();
    printArray();
    findSequence();
  }

  public Integer[] getArray() {
    return array;
  }

  public void randomInitialization() {
    array[0] = 100;
    array[array.length - 1] = 100;
    for (int i = 1; i < array.length - 1; i++) {
      array[i] = (int) (Math.random() * 2) + 1;
    }
  }

  private void printArray() {
    for (int i = 0; i < array.length; i++) {
      logger.info(array[i] + " ");
    }
    System.out.println();
  }

  private void findSequence() {
    for (int i = 1; i < array.length - 1; i++) {
      if (array[i] == array[i - 1]) {
        ++count;
        if (count > prev) {
          length = count;
          prev = count;
          end = i;
          value = array[i];
        }
      } else {
        count = 1;
        trigger = 0;
      }
    }

    printInformation();

    logger.info("sequence :");
    for (int i = end - length + 1; i <= end; i++) {
      if (end - length + 1 != 0 && end != array.length) {
        if ((end - length) < value && (end + 1) < value) {
          logger.info(array[i] + " ");
        }
      }
    }
  }

  private void printInformation() {
    logger.info(length + " <-" + " length");
    logger.info("start position :" + (end - length + 1));
    logger.info("end position :" + end);
  }
}
