package com.mushynskyi.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Minesweeper {
  private int countBomb;
  private Scanner scan = new Scanner(System.in);
  private String[][] arr;
  private String[][] arr2;
  private int randomValue;
  private Integer input;
  private String emptyString = "";
  private static Logger logger = LogManager.getLogger(Minesweeper.class);

  public Minesweeper() {
    setArraySize();
    initializeSecondArray(arr2);
    printArray(arr2);
    System.out.println();
    makeBombsAndSavePlace();
    printArray(arr);
    System.out.println();
  }

  public Minesweeper(int size) {
    setArraySize(size);
    initializeSecondArray(arr2);
    printArray(arr2);
    System.out.println();
    makeBombsAndSavePlace();
    printArray(arr);
    System.out.println();
  }

  public int getRandomValue() {
    return randomValue;
  }

  public String getEmptyString() {
    return emptyString;
  }

  public void printMinesweeper() {

    while (!emptyString.equals("*  ") && input != 0) {
      logger.info("Enter your position ");
      logger.info("Row :");
      int num1 = scan.nextInt();
      logger.info("Coll :");
      int num2 = scan.nextInt();

      if (checkGame(num1, num2)) break;
      countBombsCenterCase(num1, num2);

      countBombsLeftHighCorner(num1, num2);
      countBombsRightHighCorner(num1, num2);
      countBombsLeftDownCorner(num1, num2);
      countBombsRightDownCorner(num1, num2);
      countBombsTopSide(num1, num2);
      countBombsDownSide(num1, num2);
      countBombsRightSide(num1, num2);
      countBombsLeftSide(num1, num2);

      printArray(arr2);

      if (emptyString.equals("*  ")) {
        logger.info("Your choose, press 1 for next transaction or 0 for exit : ");
        input = scan.nextInt();
      }
    }
  }

  public boolean checkGame(int num1, int num2) {
    if (checksGameOver(num1, num2)) {
      return true;
    }
    return false;
  }

  private void setArraySize() {
    logger.info("Write size of array :");
    input = scan.nextInt();
    arr = new String[input + 2][input + 2];
    arr2 = new String[input + 2][input + 2];
  }

  private void setArraySize(int size) {
    arr = new String[size + 2][size + 2];
    arr2 = new String[size + 2][size + 2];
  }

  private void countBombsLeftSide(int num1, int num2) {
    countBomb = 0;
    if (num2 == arr.length - 1 && num1 != arr.length - 1 && num1 != 1) {
      count(arr[num1 + 1][num2]);
      count(arr[num1][num2 - 1]);
      count(arr[num1 - 1][num2]);
      count(arr[num1 - 1][num2 - 1]);
      count(arr[num1 + 1][num2 - 1]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  private void countBombsRightSide(int num1, int num2) {
    countBomb = 0;
    if (num2 == 1 && num1 != arr.length - 1 && num1 != 1) {
      count(arr[num1 + 1][num2]);
      count(arr[num1][num2 + 1]);
      count(arr[num1 - 1][num2]);
      count(arr[num1 - 1][num2 + 1]);
      count(arr[num1 + 1][num2 + 1]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  private void countBombsDownSide(int num1, int num2) {
    countBomb = 0;
    if (num1 == arr.length - 1 && num2 != arr.length - 1 && num2 != 1) {
      count(arr[num1 - 1][num2]);
      count(arr[num1][num2 - 1]);
      count(arr[num1][num2 + 1]);
      count(arr[num1 - 1][num2 - 1]);
      count(arr[num1 - 1][num2 + 1]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  private void countBombsTopSide(int num1, int num2) {
    countBomb = 0;
    if (num1 == 1 && num2 != arr.length - 1 && num2 != 1) {
      count(arr[num1 + 1][num2]);
      count(arr[num1][num2 - 1]);
      count(arr[num1][num2 + 1]);
      count(arr[num1 + 1][num2 - 1]);
      count(arr[num1 + 1][num2 + 1]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  private void countBombsRightDownCorner(int num1, int num2) {
    countBomb = 0;
    if (num1 == arr.length - 1 && num2 == arr.length - 1) {
      count(arr[num1 - 1][num2]);
      count(arr[num1 - 1][num2 - 1]);
      count(arr[num1][num2 - 1]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  private void countBombsLeftDownCorner(int num1, int num2) {
    countBomb = 0;
    if (num1 == arr.length - 1 && num2 == 1) {
      count(arr[num1][num2 + 1]);
      count(arr[num1 - 1][num2 + 1]);
      count(arr[num1 - 1][num2]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  private void countBombsRightHighCorner(int num1, int num2) {
    countBomb = 0;
    if (num1 == 1 && num2 == arr.length - 1) {
      count(arr[num1 - 1][num2 - 1]);
      count(arr[num1][num2 - 1]);
      count(arr[num1 + 1][num2]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  private void countBombsLeftHighCorner(int num1, int num2) {
    countBomb = 0;
    if (num2 == 1 && num1 == 1) {
      count(arr[num1 + 1][num2 + 1]);
      count(arr[num1 + 1][num2]);
      count(arr[num1][num2 + 1]);
      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  public void countBombsCenterCase(int num1, int num2) {
    countBomb = 0;
    if (num1 != 1 && num2 != 1 && num1 != arr.length - 1 && num2 != arr.length - 1) {
      count(arr[num1 - 1][num2]);
      count(arr[num1 + 1][num2]);
      count(arr[num1][num2 + 1]);
      count(arr[num1][num2 - 1]);

      count(arr[num1 - 1][num2 - 1]);
      count(arr[num1 + 1][num2 + 1]);
      count(arr[num1 - 1][num2 + 1]);
      count(arr[num1 + 1][num2 - 1]);

      arr2[num1][num2] = String.valueOf(countBomb) + "  ";
    }
  }

  public boolean checksGameOver(int num1, int num2) {
    if (arr[num1][num2].equals("*  ")) {
      logger.info("GAME OVER !!!!");
      emptyString = "*  ";
      arr2[num1][num2] = "*  ";
      printArray(arr2);
      return true;
    }
    return false;
  }

  private void makeBombsAndSavePlace() {
    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr.length; j++) {
        randomValue = (int) (Math.random() * 2);
        if (i == 0) {
          arr[i][j] = String.valueOf(j) + "  ";
        }
        if (j == 0) {
          System.out.println();
          arr[i][j] = String.valueOf(i) + "  ";
        }
        if (i != 0 && j != 0) {
          if (randomValue == 1) {
            arr[i][j] = "*  ";
          }
          if (randomValue == 0) {
            arr[i][j] = "^  ";
          }
        }
      }
    }
  }

  private void initializeSecondArray(String[][] arr2) {
    for (int i = 0; i < arr2.length; i++) {
      for (int j = 0; j < arr2.length; j++) {
        if (i == 0) {
          arr2[i][j] = String.valueOf(j) + "  ";
        }
        if (j == 0) {
          arr2[i][j] = String.valueOf(i + "  ");
        }
        if (i != 0 && j != 0) {
          arr2[i][j] = "-  ";
        }
      }
    }
  }

  private void printArray(String[][] arr2) {
    for (int i = 0; i < arr2.length; i++) {
      for (int j = 0; j < arr2.length; j++) {
        System.out.print(arr2[i][j] + "  ");
      }
      System.out.println();
    }
  }

  public void count(String arr) {
    if (arr.equals("*  ")) {
      ++countBomb;
    }
  }

  public int getCountBomb() {
    return countBomb;
  }
}
