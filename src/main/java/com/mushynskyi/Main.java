package com.mushynskyi;

import com.mushynskyi.view.View;

public class Main {
  public static void main(String[] args) {
    new View().showMenu();
  }
}
